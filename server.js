const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const db = require("./app/models");
const Role = db.roles;
const app = express();
require('dotenv').config()

var corsOptions = {
    origin: "http://localhost:8081"
};

app.use(cors());

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

db.sequelize.sync({ force: false }).then(() => {
    // initial();
    console.log('Drop and Resync Db');
});

require("./app/routes/users.routes")(app);
require("./app/routes/subjects.routes")(app);
require("./app/routes/auth.routes")(app);
require("./app/routes/events.routes")(app);
require("./app/routes/userEvents.routes")(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
function initial() {
    Role.create({
        id: 1,
        name: "admin",
    });

    Role.create({
        id: 2,
        name: "teacher",
    });

    Role.create({
        id: 3,
        name: "student",
    });
}
module.exports = app;