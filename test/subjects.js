//Require the dev-dependencies
const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require('../server');
const should = chai.should();

chai.use(chaiHttp);
describe('users', () => {
    beforeEach((done) => {
        //Before each test we empty the database in your case
        done();
    });
    /*
     * Test the /GET route
     */
    describe('/GET user', () => {
        it('it should GET detail of the user', (done) => {
            const id = 15;
            chai.request(server)
                .get("/api/users/" + id)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a("object");
                    res.body.should.have.property("name");
                    res.body.should.have.property("email");
                    done();
                });
        });
    });
})
