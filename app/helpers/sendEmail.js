const nodemailer = require('nodemailer')
const sgTransport = require("nodemailer-sendgrid-transport");

let otp = Math.random();
otp = otp * 1000000;
otp = parseInt(otp);

const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    service: "gmail",
    auth: {
        user: `${process.env.EMAIL_USER}`,
        pass: `${process.env.EMAIL_PASSWORD}`,
    },
});

const sendEmail = (email) => {
    // send mail with defined transport object
    const mailOptions = {
        from: `"Admin" ${process.env.EMAIL_USER}`,
        to: email,
        replyTo: `${process.env.EMAIL_USER}`,
        subject: "Xác nhận OTP",
        html:
            "<h3>Mã xác nhận OTP của bạn là</h3>" +
            "<h1 style='font-weight:bold;'>" +
            otp +
            "</h1>", // html body
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }

        return info;
    });
}

const sendEmailPassword = (email, password) => {
    const mailOptions = {
        from: `"Admin" ${process.env.EMAIL_USER}`,
        to: email,
        replyTo: `${process.env.EMAIL_USER}`,
        subject: "Thông báo tài khoản đã được khởi tạo",
        html:
            "<h3>Tài khoản của bạn đã được khởi tạo bởi Admin. Vui lòng đăng nhập với mật khẩu dưới đây và thực hiện việc đổi mật khẩu</h3>" +
            "<br />" +
            "<h1 style='font-weight:bold;'>" +
            password +
            "</h1>", // html body
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }

        return info;
    });
}

const sendEmailForgotPassword = (email, password) => {
    const mailOptions = {
        from: `"Admin" ${process.env.EMAIL_USER}`,
        to: email,
        replyTo: `${process.env.EMAIL_USER}`,
        subject: "Thông báo cập nhật mật khẩu tài khoản",
        html:
            "<h3>Mật khẩu tài khoản của bạn đã được tạo mới. Vui lòng đăng nhập với mật khẩu dưới đây và thực hiện việc đổi mật khẩu</h3>" +
            "<br />" +
            "<h1 style='font-weight:bold;'>" +
            password +
            "</h1>", // html body
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(2222)
            return console.log(error);
        }

        return info;
    });
};

const verifyCode = (otpConfirm) => {
    if (otpConfirm === otp) {
        return true;
    }
    else {
        return false;
    }
}

const resendCode = (email) => {
    const mailOptions = {
        to: email,
        subject: "Xác nhận OTP",
        html: "<h3>OTP for account verification is </h3>" + "<h1 style='font-weight:bold;'>" + otp + "</h1>" // html body
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error);
            return false;
        }
        return true;
    });
}

module.exports = {
    sendEmail,
    verifyCode,
    resendCode,
    sendEmailPassword,
    sendEmailForgotPassword,
};