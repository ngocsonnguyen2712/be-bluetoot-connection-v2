module.exports = (sequelize, Sequelize) => {
    const Events = sequelize.define("events", {
        uuid: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
    }, {
        timestamps: true,
        tableName: 'events'
    });
    Events.schema("public");
    return Events;
};