module.exports = (sequelize, Sequelize) => {
    const Subjects = sequelize.define("subjects", {
        name: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        subjectKey: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        description: {
            type: Sequelize.STRING,
        },
        semester: {
            type: Sequelize.STRING,
            allowNull: false,
        },
    }, {
        timestamps: false,
        tableName: 'subjects'
    });
    Subjects.schema("public");
    return Subjects;
};