module.exports = (sequelize, Sequelize) => {
    const Users = sequelize.define("users", {
        name: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING
        },
        password: {
            type: Sequelize.STRING
        },
        mssv: {
            type: Sequelize.STRING
        },
        dateOfBirth: {
            type: Sequelize.STRING,
        },
        active: {
            type: Sequelize.BOOLEAN
        },
        address: {
            type: Sequelize.STRING
        }
    }, {
        timestamps: false,
        tableName: 'users'
    });
    Users.schema("public");
    return Users;
};