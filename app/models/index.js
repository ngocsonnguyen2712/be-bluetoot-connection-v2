const dbConfig = require('../../config/db.config')
require("dotenv").config();
const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    operatorsAliases: false,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = require("./users.model.js")(sequelize, Sequelize);
db.roles = require("../models/role.model.js")(sequelize, Sequelize);
db.subjects = require("./subjects.model.js")(sequelize, Sequelize);
db.events = require("./events.model.js")(sequelize, Sequelize);
db.userEvents = require("./userEvent.model.js")(sequelize, Sequelize);

//user-role
db.roles.belongsToMany(db.users, {
    through: "user_roles",
    foreignKey: "roleId",
    otherKey: "userId"
});
db.users.belongsToMany(db.roles, {
    through: "user_roles",
    foreignKey: "userId",
    otherKey: "roleId"
});

//many-many users-events
db.events.belongsToMany(db.users, {
    through: "user_events",
    as: "users",
    foreignKey: "eventId",
});
db.users.belongsToMany(db.events, {
    through: "user_events",
    as: "events",
    foreignKey: "userId",
});

//relation one-many subject-event
db.subjects.hasMany(db.events, { as: "events" });
db.events.belongsTo(db.subjects, {
    foreignKey: "subjectId",
    as: "subject",
});

//relation one-many user-subject
db.users.hasMany(db.subjects, { as: "subjects" });
db.subjects.belongsTo(db.users, {
    foreignKey: "userId",
    as: "user",
});

db.ROLES = ["admin", "teacher", "student"];

module.exports = db;