module.exports = (sequelize, Sequelize) => {
    const UserEvent = sequelize.define('user_events', {
        userId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'Users',
                key: 'id'
            }
        },
        eventId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'Events',
                key: 'id'
            }
        }
    });
    return UserEvent;
};