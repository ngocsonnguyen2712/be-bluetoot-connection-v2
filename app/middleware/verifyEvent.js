const db = require("../models");
const Events = db.events;

checkDuplicateUUID = (req, res, next) => {
    // Email
    Events.findOne({
        where: {
            uuid: req.body.uuid,
        },
    }).then((event) => {
        if (
            event &&
            parseInt(event.dataValues.id) !== parseInt(req.params.id)
        ) {
            res.status(400).send({
                message: "Mã sự kiện đã tồn tại. Vui lòng thử lại",
            });
            return;
        }

        next();
    });
};

const verifyEvent = {
    checkDuplicateUUID: checkDuplicateUUID,
};

module.exports = verifyEvent;
