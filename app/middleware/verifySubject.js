const db = require("../models");
const Subjects = db.subjects;

checkDuplicateSubjectKey = (req, res, next) => {
    // Email
    Subjects.findOne({
        where: {
            subjectKey: req.body.subjectKey
        }
    }).then(subject => {
        if (subject && parseInt(subject.dataValues.id) !== parseInt(req.params.id) ) {
            res.status(400).send({
                message: "Mã môn học đã tồn tại. Vui lòng thử lại"
            });
            return;
        }

        next();
    });
};

const verifySubject = {
    checkDuplicateSubjectKey: checkDuplicateSubjectKey,
};

module.exports = verifySubject;