const db = require("../models");
const { getPagingData, getPagination } = require("../helpers/pagination");
const UserEvents = db.userEvents;
const Op = db.Sequelize.Op;

exports.create = async (req, res) => {
    const { userId, eventId } = req.body;
    const userEvent = {
        userId, eventId
    };
    try {
        const subjectResponse = await UserEvents.create(userEvent);
        if (subjectResponse) {
            res.status(200).send({ message: "Điểm danh thành công" });
        }
    } catch (err) {
        res.status(500).send({
            message:
                err.message || "Some error occurred while creating the userEvent."
        });
    }
};

exports.findAll = (req, res) => {
    const { page, size, eventId } = req.query;
    const condition = eventId
        ? { eventId: { [Op.like]: `%${eventId}%` } }
        : null;
    const { limit, offset } = getPagination(page, size);

    UserEvents.findAndCountAll({
        where: condition,
        limit,
        offset,
    })
        .then((data) => {
            const response = getPagingData(data, page, limit);
            res.send(response);
        })
        .catch((err) => {
            res.status(500).send({
                message:
                    err.message ||
                    "Some error occurred while retrieving user events.",
            });
        });
};

exports.findOne = (req, res) => {
    const id = req.params.id;

    UserEvents.findByPk(id)
        .then(data => {
            if (data) {
                res.send(data);
            } else {
                res.status(404).send({
                    message: `Cannot find userEvent with id=${id}.`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving userEvent with id=" + id
            });
        });
};