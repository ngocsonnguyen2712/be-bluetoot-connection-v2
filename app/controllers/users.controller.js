const db = require("../models");
const config = require("../../config/auth.config");
const { getPagingData, getPagination } = require("../helpers/pagination");
const Users = db.users;
const Events = db.events;
const Subjects = db.subjects
const Role = db.roles;
const Op = db.Sequelize.Op;
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const {
    sendEmail,
    verifyCode,
    sendEmailForgotPassword,
    sendEmailPassword,
} = require("../helpers/sendEmail");

exports.create = async (req, res) => {
    if (!req.body.email) {
        res.status(400).send({
            message: "Email can not be empty!",
        });
        return;
    }

    const { email, password, name } = req.body;
    const hashPassword = bcrypt.hashSync(password, 8);
    const user = {
        email,
        password: hashPassword,
        name,
        active: 1,
    };

    try {
        const userResponse = await Users.create(user);
        sendEmail(user.email);
        if (req.body.roles) {
            Role.findAll({
                where: {
                    name: {
                        [Op.or]: req.body.roles,
                    },
                },
            }).then((roles) => {
                userResponse.setRoles(roles).then(() => {
                    res.send({ message: "User was registered successfully!" });
                });
            });
        } else {
            // user role = 1
            userResponse.setRoles([3]).then(() => {
                res.send({ message: "User was registered successfully!" });
            });
        }
    } catch (err) {
        res.status(500).send({
            message:
                err.message || "Some error occurred while creating the user.",
        });
    }
};

exports.createUserByAdmin = async (req, res) => {
    const { email, roles, name, dateOfBirth, address, mssv } = req.body;
    const newPassword = Math.random().toString(20).substr(2, 8);
    const hashPassword = bcrypt.hashSync(newPassword, 8);
    const user = {
        email,
        password: hashPassword,
        roles,
        name,
        dateOfBirth,
        address,
        mssv,
        active: 1,
    };
    try {
        const userResponse = await Users.create(user);
        sendEmailPassword(email, newPassword);
        if (userResponse) {
            Role.findAll({
                where: {
                    name: {
                        [Op.or]: roles,
                    },
                },
            }).then((roles) => {
                userResponse.setRoles(roles).then(() => {
                    res.send({ message: "User was registered successfully!" });
                });
            });
        }
    } catch (err) {
        res.status(500).send({
            message:
                err.message || "Some error occurred while creating the user.",
        });
    }
};

exports.findAll = (req, res) => {
    const { page, size, name, role } = req.query;
    const condition = name ? { name: { [Op.like]: `%${name}%` } } : null;
    const { limit, offset } = getPagination(page, size);

    Users.findAndCountAll({
        attributes: ["id", "name", "email"],
        include: [
            {
                model: Role,
                where: role ? { name: role } : null,
                attributes: ["name"],
                as: "roles",
                through: {
                    attributes: [],
                },
            },
        ],
        distinct: true,
        limit: limit,
        offset: offset,
        where: condition,
    })
        .then((data) => {
            const response = getPagingData(data, page, limit);
            res.send(response);
        })
        .catch((err) => {
            res.status(500).send({
                message:
                    err.message ||
                    "Some error occurred while retrieving users.",
            });
        });
};

exports.findAllTeacher = (req, res) => {
    Users.findAndCountAll({
        attributes: ["id", "name", "email"],
        include: [
            {
                model: Role,
                where: { name: "teacher" },
                attributes: ["name"],
                as: "roles",
                through: {
                    attributes: [],
                },
            },
        ],
        distinct: true,
    })
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            res.status(500).send({
                message:
                    err.message ||
                    "Some error occurred while retrieving users.",
            });
        });
}

exports.findOne = (req, res) => {
    const id = req.params.id;

    Users.findByPk(id, {
        attributes: ["id", "name", "email", "mssv", "dateOfBirth", "address"],
        include: [
            {
                model: Role,
                attributes: ["name"],
                as: "roles",
                through: {
                    attributes: [],
                },
            },
        ],
    })
        .then((data) => {
            if (data) {
                res.send(data);
            } else {
                res.status(404).send({
                    message: `Cannot find user with id=${id}.`,
                });
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: "Error retrieving user with id=" + id,
            });
        });
};

exports.findUserByToken = (req, res) => {
    if (req.headers && req.headers["x-access-token"]) {
        const authorization = req.headers["x-access-token"];
        let decoded;
        try {
            decoded = jwt.verify(authorization, config.secret);
            // res.send(decoded);
        } catch (e) {
            return res.status(401).send("unauthorized");
        }
        const id = decoded.id;
        // Fetch the user by id
        Users.findByPk(id)
            .then((user) => {
                if (user) {
                    const authorities = [];
                    user.getRoles().then((roles) => {
                        for (let i = 0; i < roles.length; i++) {
                            authorities.push(
                                "ROLE_" + roles[i].name.toUpperCase()
                            );
                        }
                        res.status(200).send({
                            id: user.id,
                            username: user.name,
                            email: user.email,
                            roles: authorities,
                        });
                    });
                } else {
                    res.status(404).send({
                        message: `Cannot find user with id=${id}.`,
                    });
                }
            })
            .catch((err) => {
                res.status(500).send({
                    message: "Error retrieving user with id=" + id,
                });
            });
    }
    // return res.send(500);
};

exports.update = (req, res) => {
    const id = req.params.id;

    Users.update(req.body, {
        where: { id: id },
    })
        .then((num) => {
            if (num == 1) {
                res.send({
                    message: "User was updated successfully.",
                });
            } else {
                res.send({
                    message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`,
                });
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: "Error updating User with id=" + id,
            });
        });
};

exports.delete = (req, res) => {
    const id = req.params.id;

    Users.destroy({
        where: { id: id },
    })
        .then((num) => {
            if (num == 1) {
                res.send({
                    message: "User was deleted successfully!",
                });
            } else {
                res.send({
                    message: `Cannot delete User with id=${id}. Maybe User was not found!`,
                });
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: "Could not delete User with id=" + id,
            });
        });
};

exports.signIn = (req, res) => {
    const { type } = req.body;
    Users.findOne({
        where: {
            email: req.body.email,
        },
    })
        .then((user) => {
            if (!user) {
                return res
                    .status(404)
                    .send({ message: "Tài khoản không tồn tại" });
            }

            const passwordIsValid = bcrypt.compareSync(
                req.body.password,
                user.password
            );

            if (!passwordIsValid) {
                return res.status(400).send({
                    accessToken: null,
                    message: "Sai mật khẩu",
                });
            }

            const token = jwt.sign({ id: user.id }, config.secret, {
                expiresIn: 86400, // 24 hours
            });

            const authorities = [];
            user.getRoles().then((roles) => {
                if (roles.length === 1 && roles[0].name === "student" && type) {
                    return res.status(400).send({
                        accessToken: null,
                        message: "Tài khoản của bạn không có quyền truy cập",
                    });
                } else {
                    for (let i = 0; i < roles.length; i++) {
                        authorities.push("ROLE_" + roles[i].name.toUpperCase());
                    }
                    res.status(200).send({
                        id: user.id,
                        username: user.name,
                        email: user.email,
                        roles: authorities,
                        accessToken: token,
                    });
                }
            });
        })
        .catch((err) => {
            res.status(500).send({ message: err.message });
        });
};

exports.forgotPassword = (req, res) => {
    const { email } = req.body;
    const newPassword = Math.random().toString(20).substr(2, 8);
    const hashPassword = bcrypt.hashSync(newPassword, 8);
    sendEmailForgotPassword(email, newPassword);
    Users.update(
        { password: hashPassword },
        {
            where: { email: email },
        }
    )
        .then((num) => {
            if (num == 1) {
                res.send({
                    message: "User was updated successfully.",
                });
            } else {
                res.send({
                    message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`,
                });
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: "Error updating User with id=" + id,
            });
        });
}

exports.activateAccount = async (req, res, next) => {
    const confirmOTP = req.body.otp;
    const response = verifyCode(confirmOTP);
    const id = req.params.id;
    if (response) {
        Users.update(
            { active: 1 },
            {
                where: { id: id },
            }
        )
            .then((num) => {
                if (num == 1) {
                    res.send({
                        message: "User was updated successfully.",
                    });
                } else {
                    res.send({
                        message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`,
                    });
                }
            })
            .catch((err) => {
                res.status(500).send({
                    message: "Error updating User with id=" + id,
                });
            });
    } else {
        res.status(500).send({
            message: "Wrong otp",
        });
    }
};

exports.attendanceEvent = (req, res, next) => {
    const { userId, eventId } = req.body;
    Users.findByPk(userId)
        .then((user) => {
            if (!user) {
                res.status(500).send({
                    message: "User not found",
                });
            }
            Events.findByPk(eventId).then((event) => {
                if (!event) {
                    res.status(500).send({
                        message: "Event not found",
                    });
                }
                user.attendanceEvent(event);
                res.send({ message: "User has attendance successfully!" });
            });
        })
        .catch((err) => {
            res.status(500).send({
                message: "Error retrieving event with id=" + id,
            });
        });
};

exports.changePassword = async (req, res) => {
    const { oldPassword, newPassword } = req.body;
    if (req.headers && req.headers["x-access-token"]) {
        const authorization = req.headers["x-access-token"];
        let decoded;
        try {
            decoded = jwt.verify(authorization, config.secret);
        } catch (e) {
            return res.status(401).send("unauthorized");
        }
        const id = decoded.id;
        // Fetch the user by id
        Users.findByPk(id)
            .then((user) => {
                if (user) {
                    const passwordIsValid = bcrypt.compareSync(
                        oldPassword,
                        user.password
                    );

                    if (!passwordIsValid) {
                        return res.status(400).send({
                            accessToken: null,
                            message: "Mật khẩu cũ không chính xác",
                        });
                    }
                    const hashPassword = bcrypt.hashSync(newPassword, 8);
                    Users.update(
                        { password: hashPassword },
                        {
                            where: { id: user.id },
                        }
                    )
                        .then((num) => {
                            if (num == 1) {
                                res.status(200).send({
                                    message:
                                        "User password was updated successfully.",
                                });
                            } else {
                                res.send({
                                    message: `Cannot change password of User with id=${id}. Maybe User was not found or req.body is empty!`,
                                });
                            }
                        })
                        .catch((err) => {
                            res.status(500).send({
                                message: "Error updating User with id=" + id,
                            });
                        });
                } else {
                    res.status(404).send({
                        message: `Cannot find user with id=${id}.`,
                    });
                }
            })
            .catch((err) => {
                res.status(500).send({
                    message: "Error retrieving user with id=" + id,
                });
            });
    }
};

exports.findAllEventsPerUser = (req, res) => {
    const { id } = req.params;
    Users.findByPk(id, {
        include: [
            {
                model: Events,
                as: "events",
                through: {
                    attributes: ["createdAt"],
                },
                include: [
                    {
                        model: Subjects,
                        as: "subject",
                    },
                ],
            },
        ],
    })
        .then((data) => {
            if (data) {
                res.send(data.events);
            } else {
                res.status(404).send({
                    message: `Cannot find user with id=${id}.`,
                });
            }
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({
                message: "Error retrieving user with id=" + id,
            });
        });
};
