const db = require("../models");
const config = require("../../config/auth.config");
const jwt = require("jsonwebtoken");
const { getPagingData, getPagination } = require("../helpers/pagination");
const Subjects = db.subjects;
const Users = db.users;
const Op = db.Sequelize.Op;

exports.create = async (req, res) => {
    const { name, subjectKey, description, userId, semester } = req.body;
    const subject = {
        name,
        subjectKey,
        description,
        userId,
        semester,
    };
    try {
        const subjectResponse = await Subjects.create(subject);
        if (subjectResponse) {
            res.status(200).send({
                message: "Subject was created successfully !",
            });
        }
    } catch (err) {
        res.status(500).send({
            message:
                err.message ||
                "Some error occurred while creating the subject.",
        });
    }
};

exports.findAll = (req, res) => {
    const { page, size, isAdmin } = req.query;
    const { limit, offset } = getPagination(page, size);
    const authorization = req.headers["x-access-token"];
    let decoded;
    try {
        decoded = jwt.verify(authorization, config.secret);
    } catch (e) {
        return res.status(401).send("unauthorized");
    }
    const userId = decoded.id;
    const condition = isAdmin === "false" ? { userId: userId } : null;
    Subjects.findAndCountAll({
        include: [
            {
                model: Users,
                attributes: ["name"],
                as: "user",
            },
        ],
        distinct: true,
        limit: limit,
        offset: offset,
        where: condition,
    })
        .then((data) => {
            const response = getPagingData(data, page, limit);
            res.send(response);
        })
        .catch((err) => {
            res.status(500).send({
                message:
                    err.message ||
                    "Some error occurred while retrieving subjects.",
            });
        });
};

exports.findAllSubjectsWithName = (req, res) => {
    const { name } = req.query;
    const condition = name
        ? {
              [Op.or]: [
                  {
                      name: {
                          [Op.like]: `%${name}%`,
                      },
                  },
                  {
                      subjectKey: {
                          [Op.like]: `%${name}%`,
                      },
                  },
              ],
          }
        : null;
    Subjects.findAll({
        distinct: true,
        where: condition,
    })
        .then((data) => {
            // const response = getPagingData(data, page, limit);
            res.send(data);
        })
        .catch((err) => {
            res.status(500).send({
                message:
                    err.message ||
                    "Some error occurred while retrieving subjects.",
            });
        });
};

exports.findOne = (req, res) => {
    const id = req.params.id;

    Subjects.findByPk(id, {
        include: [
            {
                model: Users,
                attributes: ["name"],
                as: "user",
            },
        ],
    })
        .then((data) => {
            if (data) {
                res.send(data);
            } else {
                res.status(404).send({
                    message: `Cannot find subject with id=${id}.`,
                });
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: "Error retrieving subject with id=" + id,
            });
        });
};

exports.update = (req, res) => {
    const id = req.params.id;
    Subjects.update(req.body, {
        where: { id: id },
    })
        .then((num) => {
            if (num == 1) {
                res.send({
                    message: "Subject was updated successfully.",
                });
            } else {
                res.send({
                    message: `Cannot update subject with id=${id}. Maybe subject was not found or req.body is empty!`,
                });
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: "Error updating subject with id=" + id,
            });
        });
};

exports.delete = (req, res) => {
    const id = req.params.id;

    Subjects.destroy({
        where: { id: id },
    })
        .then((num) => {
            if (num == 1) {
                res.send({
                    message: "Subject was deleted successfully!",
                });
            } else {
                res.send({
                    message: `Cannot delete subject with id=${id}. Maybe subject was not found!`,
                });
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: "Could not delete subject with id=" + id,
            });
        });
};
