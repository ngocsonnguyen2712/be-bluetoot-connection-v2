const excel = require("exceljs");
const moment = require("moment");
const db = require("../models");
const { getPagingData, getPagination } = require("../helpers/pagination");
const {} = require("../models");
const Events = db.events;
const Users = db.users;
const Subjects = db.subjects;
const Op = db.Sequelize.Op;
const config = require("../../config/auth.config");
const jwt = require("jsonwebtoken");

exports.create = async (req, res) => {
    const { uuid, subjectId } = req.body;
    const event = {
        uuid,
        subjectId,
    };
    try {
        const subjectResponse = await Events.create(event);
        if (subjectResponse) {
            res.status(200).send(subjectResponse);
        }
    } catch (err) {
        res.status(500).send({
            message:
                err.message || "Some error occurred while creating the event.",
        });
    }
};

exports.findAll = (req, res) => {
    const { page, size, isAdmin } = req.query;
    const { limit, offset } = getPagination(page, size);
    const authorization = req.headers["x-access-token"];
    let decoded;
    try {
        decoded = jwt.verify(authorization, config.secret);
    } catch (e) {
        return res.status(401).send("unauthorized");
    }
    const userId = decoded.id;
    const condition = isAdmin === "false" ? { userId: userId } : null;

    if (isAdmin !== "false") {
        Events.findAndCountAll({
            include: ["subject"],
            distinct: true,
            limit: limit,
            offset: offset,
        })
            .then((data) => {
                const response = getPagingData(data, page, limit);
                res.send(response);
            })
            .catch((err) => {
                res.status(500).send({
                    message:
                        err.message ||
                        "Some error occurred while retrieving events.",
                });
            });
    } else {
        Subjects.findAll({
            distinct: true,
            where: condition,
        })
            .then((data) => {
                const listSubjectId = data.map((item) => item.id);
                Events.findAndCountAll({
                    include: ["subject"],
                    distinct: true,
                    limit: limit,
                    offset: offset,
                    where: {
                        subjectId: {
                            [Op.or]: listSubjectId,
                        },
                    },
                })
                    .then((dataEvents) => {
                        const response = getPagingData(dataEvents, page, limit);
                        res.send(response);
                    })
                    .catch((err) => {
                        res.status(500).send({
                            message:
                                err.message ||
                                "Some error occurred while retrieving events.",
                        });
                    });
            })
            .catch((err) => {
                res.status(500).send({
                    message:
                        err.message ||
                        "Some error occurred while retrieving subjects.",
                });
            });
    }
};

exports.findOne = (req, res) => {
    const id = req.params.id;
    Events.findByPk(id, { include: ["subject"] })
        .then((data) => {
            if (data) {
                res.send(data);
            } else {
                res.status(404).send({
                    message: `Cannot find event with id=${id}.`,
                });
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: "Error retrieving event with id=" + id,
            });
        });
};

exports.findOneWithUsers = (req, res) => {
    const id = req.params.id;

    Events.findByPk(id, {
        include: [
            {
                model: Users,
                as: "users",
                attributes: ["id", "name", "mssv"],
                through: {
                    attributes: ["createdAt"],
                },
            },
            {
                model: Subjects,
                as: "subject",
                attributes: ["name"],
            },
        ],
    })
        .then((data) => {
            if (data) {
                res.send(data);
            } else {
                res.status(404).send({
                    message: `Cannot find event with id=${id}.`,
                });
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: "Error retrieving event with id=" + id,
            });
        });
};

exports.update = (req, res) => {
    const id = req.params.id;

    Events.update(req.body, {
        where: { id: id },
    })
        .then((num) => {
            if (num == 1) {
                res.send({
                    message: "Event was updated successfully.",
                });
            } else {
                res.send({
                    message: `Cannot update event with id=${id}. Maybe event was not found or req.body is empty!`,
                });
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: "Error updating event with id=" + id,
            });
        });
};

exports.delete = (req, res) => {
    const id = req.params.id;

    Events.destroy({
        where: { id: id },
    })
        .then((num) => {
            if (num == 1) {
                res.send({
                    message: "Event was deleted successfully!",
                });
            } else {
                res.send({
                    message: `Cannot delete event with id=${id}. Maybe event was not found!`,
                });
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: "Could not delete event with id=" + id,
            });
        });
};

exports.download = (req, res) => {
    const id = req.params.id;

    Events.findByPk(id, {
        include: [
            {
                model: Users,
                as: "users",
                attributes: ["id", "name", "mssv"],
                through: {
                    attributes: ["createdAt"],
                },
            },
            {
                model: Subjects,
                as: "subject",
                attributes: ["name"],
            },
        ],
    }).then((data) => {
        let listUsers = [];
        data.users.forEach((item) => {
            listUsers.push({
                id: item.id,
                name: item.name,
                mssv: item.mssv,
                createdAt: moment(item.user_events.createdAt).format(
                    "DD-MM-YYYY HH:mm:ss"
                ),
            });
        });
        let workbook = new excel.Workbook();
        let worksheet = workbook.addWorksheet(
            `${data.subject.name}_${moment(data.createdAt).format(
                "DD-MM-YYYY"
            )}`
        );
        worksheet.columns = [
            { header: "Id", key: "id", width: 5 },
            { header: "Họ và tên", key: "name", width: 30 },
            { header: "Mã số sinh viên", key: "mssv", width: 30 },
            { header: "Thời gian điểm danh", key: "createdAt", width: 50 },
        ];
        worksheet.addRows(listUsers);

        res.setHeader(
            "Content-Type",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        );
        res.setHeader(
            "Content-Disposition",
            "attachment; filename=" + "diem_danh.xlsx"
        );
        res.setHeader("Access-Control-Expose-Headers", "Content-Disposition");

        return workbook.xlsx.write(res).then(function () {
            res.status(200).end();
        });
    });
};

exports.findEventOfSubjectInCurrentDay = (req, res) => {
    const { subjectId, date } = req.query;
    Events.findOne({
        where: {
            subjectId: subjectId,
            createdAt: {
                [Op.gte]: moment().subtract(1, "hours").toDate(),
                [Op.lte]: moment().toDate()
            },
        },
    })
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            res.status(500).send({
                message:
                    err.message ||
                    "Some error occurred while retrieving users.",
            });
        });
}