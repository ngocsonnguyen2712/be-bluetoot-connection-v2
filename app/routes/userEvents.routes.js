const userEvents = require("../controllers/userEvent.controller.js");
const { authJwt } = require("../middleware");

module.exports = app => {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    // attendance a event
    app.post(
        "/api/user-attendance",
        [authJwt.verifyToken],
        userEvents.create
    )

    // Retrieve all userEvents
    app.get(
        "/api/user-attendance",
        [authJwt.verifyToken, authJwt.isTeacherOrAdmin],
        userEvents.findAll
    );

    // Retrieve a single subject with id
    app.get(
        "/api/user-attendance/:id",
        [authJwt.verifyToken],
        userEvents.findOne
    );
};