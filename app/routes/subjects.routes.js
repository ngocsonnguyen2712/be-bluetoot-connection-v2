const subjects = require("../controllers/subjects.controller.js");
const { authJwt } = require("../middleware");
const verifySubject = require("../middleware/verifySubject.js");

module.exports = app => {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    // Create new subject
    app.post(
        "/api/subjects",
        [authJwt.verifyToken, authJwt.isAdmin, verifySubject.checkDuplicateSubjectKey],
        subjects.create
    );

    // Retrieve all subjects
    app.get(
        "/api/subjects",
        [authJwt.verifyToken, authJwt.isTeacherOrAdmin],
        subjects.findAll
    );

    app.get(
        "/api/subjects/all",
        [authJwt.verifyToken],
        subjects.findAllSubjectsWithName
    )

    // Retrieve a single subject with id
    app.get(
        "/api/subjects/:id",
        [authJwt.verifyToken],
        subjects.findOne
    );

    // Update a subject with id
    app.put(
        "/api/subjects/:id",
        [authJwt.verifyToken, authJwt.isAdmin, verifySubject.checkDuplicateSubjectKey],
        subjects.update
    );

    // Delete a subject with id
    app.delete(
        "/api/subjects/:id",
        [authJwt.verifyToken, authJwt.isAdmin],
        subjects.delete
    );
};