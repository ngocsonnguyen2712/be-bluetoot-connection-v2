const events = require("../controllers/events.controller.js");
const { authJwt } = require("../middleware");
const verifyEvent = require("../middleware/verifyEvent.js");

module.exports = app => {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    // Create new event
    app.post(
        "/api/events",
        [authJwt.verifyToken, authJwt.isTeacherOrAdmin, verifyEvent.checkDuplicateUUID],
        events.create
    );

    // Retrieve all events
    app.get(
        "/api/events",
        [authJwt.verifyToken, authJwt.isTeacherOrAdmin],
        events.findAll
    );

    // Retrieve a single event with id
    app.get(
        "/api/events/:id",
        [authJwt.verifyToken],
        events.findOne
    );

    // Retrieve a single event with user data
    app.get(
        "/api/events-users/:id",
        [authJwt.verifyToken, authJwt.isTeacherOrAdmin],
        events.findOneWithUsers
    )

    // Update a event with id
    app.put(
        "/api/events/:id",
        [authJwt.verifyToken, authJwt.isTeacherOrAdmin],
        events.update
    );

    // Delete a event with id
    app.delete(
        "/api/events/:id",
        [authJwt.verifyToken, authJwt.isTeacherOrAdmin],
        events.delete
    );

    app.get(
        "/api/download/:id",
        [authJwt.verifyToken, authJwt.isTeacherOrAdmin],
        events.download
    );

    app.get(
        "/api/check-current-event/",
        [authJwt.verifyToken],
        events.findEventOfSubjectInCurrentDay
    );
};