const users = require("../controllers/users.controller.js");
const userEvents = require("../controllers/userEvent.controller.js");
const { authJwt, verifySignUp } = require("../middleware");

module.exports = app => {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    // Create new User by Admin
    app.post(
        "/api/users",
        [
            authJwt.verifyToken,
            authJwt.isAdmin,
            verifySignUp.checkDuplicateEmail,
            verifySignUp.checkRolesExisted,
        ],
        users.createUserByAdmin
    );

    // Retrieve all users with paging
    app.get(
        "/api/users",
        [authJwt.verifyToken, authJwt.isTeacherOrAdmin],
        users.findAll
    );

    // Retrieve all users role teacher no paging
    app.get(
        "/api/users-role-teacher",
        [authJwt.verifyToken, authJwt.isAdmin],
        users.findAllTeacher
    );

    // Retrieve a single user with id
    app.get("/api/users/:id", [authJwt.verifyToken], users.findOne);

    // Retrieve a single user with token
    app.get(
        "/api/user",
        // [authJwt.verifyToken],
        users.findUserByToken
    );

    // Update a user with id
    app.put("/api/users/:id", [authJwt.verifyToken], users.update);

    app.put("/api/users/activated/:id", users.activateAccount);

    // Delete a user with id
    app.delete(
        "/api/users/:id",
        [authJwt.verifyToken, authJwt.isAdmin],
        users.delete
    );

    // attendance a event
    app.post("/api/users/attendance", [authJwt.verifyToken], userEvents.create);

    //change password
    app.post(
        "/api/users/change-password",
        [authJwt.verifyToken],
        users.changePassword
    );

    //forgot password
    app.post(
        "/api/users/forgot-password",
        users.forgotPassword
    );

    // checking history of student
    app.get(
        "/api/checking-history/:id",
        [authJwt.verifyToken],
        users.findAllEventsPerUser
    );
};